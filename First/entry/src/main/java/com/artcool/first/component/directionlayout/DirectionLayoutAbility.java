package com.artcool.first.component.directionlayout;

import com.artcool.first.component.directionlayout.slice.DirectionLayoutAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class DirectionLayoutAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DirectionLayoutAbilitySlice.class.getName());
    }
}
