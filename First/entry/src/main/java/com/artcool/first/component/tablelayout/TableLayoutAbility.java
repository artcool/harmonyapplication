package com.artcool.first.component.tablelayout;

import com.artcool.first.component.tablelayout.slice.TableLayoutAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class TableLayoutAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TableLayoutAbilitySlice.class.getName());
    }
}
