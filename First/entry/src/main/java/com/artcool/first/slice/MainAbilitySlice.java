package com.artcool.first.slice;

import com.artcool.first.component.DatePicker.slice.DatePickerAbilitySlice;
import com.artcool.first.ResourceTable;
import com.artcool.first.component.checkbox.slice.CheckBoxAbilitySlice;
import com.artcool.first.component.componententry.slice.ComponentEntryAbilitySlice;
import com.artcool.first.component.dependentlayout.slice.DependentLayoutSlice;
import com.artcool.first.component.directionlayout.slice.DirectionLayoutAbilitySlice;
import com.artcool.first.component.listcontainer.slice.ListContainerAbilitySlice;
import com.artcool.first.component.radiocontainer.slice.RadioContainerAbilitySlice;
import com.artcool.first.component.stacklayout.slice.StackLayoutAbilitySlice;
import com.artcool.first.component.tablelayout.slice.TableLayoutAbilitySlice;
import com.artcool.first.component.tablist.slice.TabListAbilitySlice;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice {


    private Button component;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
        initListener();
    }

    private void initListener() {
        component.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new ComponentEntryAbilitySlice(),new Intent());
            }
        });
    }

    private void initView() {
        component = (Button) findComponentById(ResourceTable.Id_component_button);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
