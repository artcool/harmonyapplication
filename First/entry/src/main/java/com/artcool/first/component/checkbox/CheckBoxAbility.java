package com.artcool.first.component.checkbox;

import com.artcool.first.component.checkbox.slice.CheckBoxAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CheckBoxAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CheckBoxAbilitySlice.class.getName());
    }
}
