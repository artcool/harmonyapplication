package com.artcool.first.component.listcontainer.provider;

import com.artcool.first.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.Optional;

public class NewsTypeProvider extends BaseItemProvider {
    private String[] newsTypeLists;
    private Context context;

    public NewsTypeProvider(String[] newsTypeLists, Context context) {
        this.newsTypeLists = newsTypeLists;
        this.context = context;
    }

    @Override
    public int getCount() {
        return newsTypeLists == null ? 0 : newsTypeLists.length;
    }

    @Override
    public Object getItem(int i) {
        return Optional.of(newsTypeLists[i]);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder holder = null;
        if (component == null) {
            component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_news_type_layout,componentContainer,false);
            holder = new ViewHolder();
            Component componentText = component.findComponentById(ResourceTable.Id_news_type_text);
            if (componentText instanceof Text) {
                holder.title = (Text) componentText;
            }
            component.setTag(holder);
        } else {
            if (component.getTag() instanceof ViewHolder) {
                holder = (ViewHolder) component.getTag();
            }
        }
        if (holder != null) {
            holder.title.setText(newsTypeLists[i]);
        }
        return component;
    }

    private static class ViewHolder{
        Text title;
    }
}
