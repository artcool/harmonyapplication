package com.artcool.first.component.checkbox.slice;

import com.artcool.first.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;

import java.util.HashSet;

public class CheckBoxAbilitySlice extends AbilitySlice {

    private Text answer;
    private HashSet selectedSet = new HashSet();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_check_box);
        initView();
    }

    private void initView() {
        answer = (Text) findComponentById(ResourceTable.Id_checkbox_answer_text);
        Checkbox checkBoxOne = (Checkbox) findComponentById(ResourceTable.Id_checkbox_one);
        checkBoxOne.setBubbleElement(getStateElement());
        setCheckedStateChangeListener(checkBoxOne,"A");
        if (checkBoxOne.isChecked()) {
            selectedSet.add("A");
        }
        Checkbox checkBoxTwo = (Checkbox) findComponentById(ResourceTable.Id_checkbox_two);
        checkBoxTwo.setBubbleElement(getStateElement());
        setCheckedStateChangeListener(checkBoxTwo,"B");
        if (checkBoxTwo.isChecked()) {
            selectedSet.add("B");
        }
        Checkbox checkBoxThree = (Checkbox) findComponentById(ResourceTable.Id_checkbox_three);
        checkBoxThree.setBubbleElement(getStateElement());
        setCheckedStateChangeListener(checkBoxThree,"C");
        if (checkBoxThree.isChecked()) {
            selectedSet.add("C");
        }
        Checkbox checkBoxFour = (Checkbox) findComponentById(ResourceTable.Id_checkbox_four);
        checkBoxFour.setBubbleElement(getStateElement());
        setCheckedStateChangeListener(checkBoxFour,"D");
        if (checkBoxFour.isChecked()) {
            selectedSet.add("D");
        }
    }

    private void setCheckedStateChangeListener(Checkbox checkBox, String value) {
        checkBox.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                if (b) {
                    selectedSet.add(value);
                } else {
                    selectedSet.remove(value);
                }
                showAnswer();
            }
        });
    }

    private void showAnswer() {
        String select = selectedSet.toString();
        answer.setText(select);
    }

    private Element getStateElement() {
        ShapeElement elementButtonOn = new ShapeElement();
        elementButtonOn.setRgbColor(RgbPalette.RED);
        elementButtonOn.setShape(ShapeElement.OVAL);
        ShapeElement elementButtonOff = new ShapeElement();
        elementButtonOff.setRgbColor(RgbPalette.WHITE);
        elementButtonOff.setShape(ShapeElement.OVAL);
        StateElement checkElement = new StateElement();
        checkElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED},elementButtonOn);
        checkElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY},elementButtonOff);
        return checkElement;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
