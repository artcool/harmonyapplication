package com.artcool.first.component.tablelayout.slice;

import com.artcool.first.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.TableLayout;
import ohos.agp.components.Text;

public class TableLayoutAbilitySlice extends AbilitySlice {

    private Text info;
    private Button call;
    private Button clear;
    private TableLayout table;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_table_layout);
        initView();
        initListener();
    }

    private void initListener() {
        call.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

            }
        });
        clear.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                info.setText("");
            }
        });
        TableLayout table = (TableLayout) findComponentById(ResourceTable.Id_table);
        int childCount = table.getChildCount();
        for (int i = 0; i < childCount; i++) {
            setClickListener(table,i);
        }

    }

    private void setClickListener(TableLayout table, int i) {
        Button child = (Button) table.getComponentAt(i);
        child.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (component instanceof Button) {
                    Button button = (Button) component;
                    info.setText(info.getText() + button.getText());
                }
            }
        });
    }

    private void initView() {
        info = (Text) findComponentById(ResourceTable.Id_info);
        call = (Button) findComponentById(ResourceTable.Id_call);
        clear = (Button) findComponentById(ResourceTable.Id_clear);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
