package com.artcool.first.component.listcontainer;

import com.artcool.first.component.listcontainer.slice.ListContainerAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ListContainerAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ListContainerAbilitySlice.class.getName());
    }
}
