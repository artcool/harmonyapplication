package com.artcool.first.component.radiocontainer.slice;

import com.artcool.first.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;

import java.util.Locale;

public class RadioContainerAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_radio_container);
        initView();
    }

    private void initView() {
        Text answer = (Text) findComponentById(ResourceTable.Id_answer_text);
        RadioContainer container = (RadioContainer) findComponentById(ResourceTable.Id_radio_container);
        int childCount = container.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ((RadioButton)container.getComponentAt(i)).setButtonElement(getStateElement());
        }
        container.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                answer.setText(String.format(Locale.ROOT,"[%c]",(char)('A' + i)));
            }
        });
    }

    /**
     * 定义radiobutton的背景
     * @return
     */
    private Element getStateElement() {
        ShapeElement elementButtonOn = new ShapeElement();
        elementButtonOn.setRgbColor(RgbPalette.RED);
        elementButtonOn.setShape(ShapeElement.OVAL);

        ShapeElement elementButtonOff = new ShapeElement();
        elementButtonOff.setRgbColor(RgbPalette.DARK_GRAY);
        elementButtonOff.setShape(ShapeElement.OVAL);

        StateElement stateElement = new StateElement();
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED},elementButtonOn);
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY},elementButtonOff);
        return stateElement;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
