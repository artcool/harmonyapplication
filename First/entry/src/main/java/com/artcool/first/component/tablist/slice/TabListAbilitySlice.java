package com.artcool.first.component.tablist.slice;

import com.artcool.first.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.TabList;
import ohos.agp.components.Text;

public class TabListAbilitySlice extends AbilitySlice {

    private TabList tabList;
    private Text tabContent;
    private static final int TAB_WIDTH = 64;
    private static final int TAB_PADDING_LEFT = 12;
    private static final int TAB_PADDING_TOP = 12;
    private static final int TAB_PADDING_RIGHT = 12;
    private static final int TAB_PADDING_BOTTOM = 12;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_tab_list);
        initView();
        initTab();
        addTabListener();
        addGuestureListener();
    }

    private void addGuestureListener() {
       
    }

    private void addTabListener() {
        tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                tabContent.setText("选中了："+tab.getText());
            }

            @Override
            public void onUnselected(TabList.Tab tab) {

            }

            @Override
            public void onReselected(TabList.Tab tab) {

            }
        });
    }

    private void initTab() {
        if (tabList.getTabCount() == 0) {
            tabList.addTab(createTab("tab1"));
            tabList.addTab(createTab("tab2"));
            tabList.addTab(createTab("tab3"));
            tabList.setFixedMode(true);
            tabList.getTabAt(0).select();
            tabContent.setText("选中了：" + tabList.getTabAt(0).getText());
        }
    }

    private TabList.Tab createTab(String name) {
        TabList.Tab tab = tabList.new Tab(this);
        tab.setText(name);
        tab.setMinWidth(TAB_WIDTH);
        tab.setPadding(TAB_PADDING_LEFT,TAB_PADDING_TOP,TAB_PADDING_RIGHT,TAB_PADDING_BOTTOM);
        return tab;
    }

    private void initView() {
        tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);
        tabContent = (Text) findComponentById(ResourceTable.Id_tab_content);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
