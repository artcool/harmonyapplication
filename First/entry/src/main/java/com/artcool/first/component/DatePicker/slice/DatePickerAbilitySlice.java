package com.artcool.first.component.DatePicker.slice;

import com.artcool.first.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DatePicker;
import ohos.agp.components.Text;

import java.util.Locale;

public class DatePickerAbilitySlice extends AbilitySlice {

    private DatePicker datePick;
    private Text dateText;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_date_picker);
        initView();
        initDate();
        initListener();
    }

    private void initListener() {
        datePick.setValueChangedListener(new DatePicker.ValueChangedListener() {
            @Override
            public void onValueChanged(DatePicker datePicker, int year, int month, int day) {
                dateText.setText(String.format(Locale.ROOT,"%02d/%02d/%4d",day,month,year));
            }
        });
    }

    private void initDate() {
        int day = datePick.getDayOfMonth();
        int month = datePick.getMonth();
        int year = datePick.getYear();
        dateText.setText(String.format(Locale.ROOT,"%02d/%02d/%4d",day,month,year));
    }

    private void initView() {
        datePick = (DatePicker) findComponentById(ResourceTable.Id_date_pick);
        dateText = (Text) findComponentById(ResourceTable.Id_date_text);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
