package com.artcool.first.component.stacklayout;

import com.artcool.first.component.stacklayout.slice.StackLayoutAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class StackLayoutAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(StackLayoutAbilitySlice.class.getName());
    }
}
