package com.artcool.first.component.componententry;

import com.artcool.first.component.componententry.slice.ComponentEntryAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ComponentEntryAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ComponentEntryAbilitySlice.class.getName());
    }
}
