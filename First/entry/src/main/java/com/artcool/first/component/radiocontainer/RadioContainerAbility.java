package com.artcool.first.component.radiocontainer;

import com.artcool.first.component.radiocontainer.slice.RadioContainerAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class RadioContainerAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RadioContainerAbilitySlice.class.getName());
    }
}
