package com.artcool.first.component.dependentlayout;

import com.artcool.first.component.dependentlayout.slice.DependentLayoutSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class DependentLayout extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DependentLayoutSlice.class.getName());
    }
}
