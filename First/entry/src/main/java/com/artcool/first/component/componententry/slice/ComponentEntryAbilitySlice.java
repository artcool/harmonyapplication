package com.artcool.first.component.componententry.slice;

import com.artcool.first.ResourceTable;
import com.artcool.first.component.DatePicker.slice.DatePickerAbilitySlice;
import com.artcool.first.component.checkbox.slice.CheckBoxAbilitySlice;
import com.artcool.first.component.dependentlayout.slice.DependentLayoutSlice;
import com.artcool.first.component.directionlayout.slice.DirectionLayoutAbilitySlice;
import com.artcool.first.component.listcontainer.slice.ListContainerAbilitySlice;
import com.artcool.first.component.radiocontainer.slice.RadioContainerAbilitySlice;
import com.artcool.first.component.stacklayout.slice.StackLayoutAbilitySlice;
import com.artcool.first.component.tablelayout.slice.TableLayoutAbilitySlice;
import com.artcool.first.component.tablist.slice.TabListAbilitySlice;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class ComponentEntryAbilitySlice extends AbilitySlice {

    private Button tabList;
    private Button listContainer;
    private Button radioContainer;
    private Button checkbox;
    private Button datePick;
    private Button direction;
    private Button dependent;
    private Button stackLayout;
    private Button tableLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_component_entry);
        initView();
        initListener();
    }

    private void initListener() {
        tabList.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new TabListAbilitySlice(), new Intent());
            }
        });
        listContainer.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new ListContainerAbilitySlice(), new Intent());
            }
        });
        radioContainer.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new RadioContainerAbilitySlice(), new Intent());
            }
        });
        checkbox.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new CheckBoxAbilitySlice(), new Intent());
            }
        });
        datePick.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new DatePickerAbilitySlice(), new Intent());
            }
        });

        direction.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new DirectionLayoutAbilitySlice(), new Intent());
            }
        });
        dependent.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new DependentLayoutSlice(), new Intent());
            }
        });
        stackLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new StackLayoutAbilitySlice(), new Intent());
            }
        });
        tableLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new TableLayoutAbilitySlice(), new Intent());
            }
        });
    }

    private void initView() {
        tabList = (Button) findComponentById(ResourceTable.Id_tab_list);
        listContainer = (Button) findComponentById(ResourceTable.Id_list_container);
        radioContainer = (Button) findComponentById(ResourceTable.Id_radio_container);
        checkbox = (Button) findComponentById(ResourceTable.Id_checkbox);
        datePick = (Button) findComponentById(ResourceTable.Id_date_picker);
        direction = (Button) findComponentById(ResourceTable.Id_directional_layout);
        dependent = (Button) findComponentById(ResourceTable.Id_dependent_layout);
        stackLayout = (Button) findComponentById(ResourceTable.Id_stack_layout);
        tableLayout = (Button) findComponentById(ResourceTable.Id_table_layout);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
