package com.artcool.first.component.listcontainer.provider;

import com.artcool.first.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;
import java.util.Optional;

public class NewsListProvider extends BaseItemProvider {

    private List<NewsInfo> newsList;
    private Context context;

    public NewsListProvider(List<NewsInfo> newsList, Context context) {
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return newsList == null?0:newsList.size();
    }

    @Override
    public Object getItem(int i) {
        return Optional.of(newsList.get(i));
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder holder = null;
        if (component == null) {
            component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_news_layout,componentContainer,false);
            holder = new ViewHolder();
            Component componentText = component.findComponentById(ResourceTable.Id_item_news_title);
            Component componentImage = component.findComponentById(ResourceTable.Id_item_news_image);
            if (componentText instanceof Text) {
                holder.title = (Text) componentText;
            }
            if (componentImage instanceof Image) {
                holder.image = (Image) componentImage;
            }
            component.setTag(holder);
        } else {
            if (component.getTag() instanceof ViewHolder) {
                holder = (ViewHolder) component.getTag();
            }
        }
        if (holder != null) {
            holder.title.setText(newsList.get(i).getTitle());
            holder.image.setScaleMode(Image.ScaleMode.STRETCH);
        }
        return component;
    }

    private static class ViewHolder{
        Text title;
        Image image;
    }
}
