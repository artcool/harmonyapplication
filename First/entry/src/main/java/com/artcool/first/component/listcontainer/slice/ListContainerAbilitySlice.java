package com.artcool.first.component.listcontainer.slice;

import com.artcool.first.ResourceTable;
import com.artcool.first.component.listcontainer.provider.NewsInfo;
import com.artcool.first.component.listcontainer.provider.NewsListProvider;
import com.artcool.first.component.listcontainer.provider.NewsTypeProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

import java.util.ArrayList;
import java.util.List;

public class ListContainerAbilitySlice extends AbilitySlice {

    private ListContainer newsTypeList;
    private ListContainer newsContentList;
    private NewsTypeProvider newsTypeProvider;
    private NewsListProvider newsListProvider;
    private Text selectText;
    private List<NewsInfo> selectNews;
    private List<NewsInfo> totalNews;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_list_container);
        initView();
        initProvider();
        setListContainer();
        initListener();
    }

    private void initListener() {
        newsTypeList.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                setCategoryFocus(false);
                Component newsTypeText = (Text) component.findComponentById(ResourceTable.Id_news_type_text);
                if (newsTypeText instanceof Text) {
                    selectText = (Text) newsTypeText;
                }
                setCategoryFocus(true);
                selectNews.clear();
                if (i == 0) {
                    selectNews.addAll(totalNews);
                } else {
                    setNewsData();
                }
                updateListView();
            }
        });
        newsTypeList.setSelected(true);
        newsTypeList.setSelectedItemIndex(0);
    }

    private void updateListView() {
        newsListProvider.notifyDataChanged();
        newsContentList.invalidate();
        newsContentList.scrollToCenter(0);
    }

    private void setNewsData() {
        String newsType = selectText.getText();
        for (NewsInfo newsInfo:totalNews) {
            if (newsType.equals(newsInfo.getType())) {
                selectNews.add(newsInfo);
            }
        }
    }

    private void setCategoryFocus(boolean isFocus) {
        if (selectText == null) {
            return;
        }

        if (isFocus) {
            selectText.setTextColor(new Color(Color.getIntColor("#afaafa")));
            selectText.setScaleX(1.2f);
            selectText.setScaleY(1.2f);
        } else {
            selectText.setTextColor(new Color(Color.getIntColor("#666666")));
            selectText.setScaleX(1.0f);
            selectText.setScaleY(1.0f);
        }
    }

    /**
     * 设置适配器
     */
    private void setListContainer() {
        newsContentList.setItemProvider(newsListProvider);
        newsTypeList.setItemProvider(newsTypeProvider);
    }

    /**
     * 初始化数据
     */
    private void initProvider() {
        String[] listTypeNames = new String[]{"全部","推荐","关注","NBA","奥运"};
        newsTypeProvider = new NewsTypeProvider(listTypeNames,this);
        newsTypeProvider.notifyDataChanged();

        String[] newsTypes = new String[]{"全部","推荐","关注","奥运","健康", "金融", "技术", "运动"
                , "互联网", "游戏"};
        String[] newsTitles = new String[]{
                "Best Enterprise Wi-Fi Network Award of the Wireless Broadband Alliance 2020",
                "Openness and Cooperation Facilitate Industry Upgrade",
                "High-voltage super-fast charging is an inevitable trend",
                "Ten Future Trends of Digital Energy",
                "Ascend Helps Industry, Learning, and Research Promote AI Industry "
                        + "Development in the National AI Contest",
                "Enterprise data centers are moving towards autonomous driving network",
                "One optical fiber lights up a green smart room",
                "Trust technology, embrace openness, and share the world prosperity brought by technology",
                "Intelligent Twins Won the Leading Technology Achievement Award at the 7th World Internet Conference",
                "Maximizing the Value of Wireless Networks and Ushering in the Golden Decade of 5G"
        };
        totalNews = new ArrayList<>();
        selectNews = new ArrayList<>();
        for (int i = 0; i < newsTypes.length; i++) {
            NewsInfo newsInfo = new NewsInfo();
            newsInfo.setTitle(newsTitles[i]);
            newsInfo.setType(newsTypes[i]);
            totalNews.add(newsInfo);
        }
        selectNews.addAll(totalNews);
        newsListProvider = new NewsListProvider(selectNews,this);
        newsListProvider.notifyDataChanged();

    }

    private void initView() {
        newsTypeList = (ListContainer) findComponentById(ResourceTable.Id_news_type_list);
        newsContentList = (ListContainer) findComponentById(ResourceTable.Id_news_content_list);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
